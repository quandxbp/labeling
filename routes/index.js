var express = require("express"),
    _ = require('lodash'),
    service = require('../services/tools');
helper = require('../helper/support');
var mongoose = require('mongoose');

var router = express.Router();
var Comment = require("../models/comment");

router.get("/", (req, res) => {
    return res.render("landing");
});

router.get("/find", (req, res) => {
    let id = req.query.id;
    let perPage = Math.abs(req.query.limit) || 100;
    let page = (Math.abs(req.query.page) || 1) - 1;
    // request by http://localhost:3000/label?page=1&limit=100

    if(id == null) {
        return res.send("Please try again");
    }

    Comment.findById(id, function (err, comments) {
            if (err) return res.send("Sai format rồi.");
            Comment.countDocuments().exec(function (err, count) {
                if (err) return res.send("Sai format rồi.");
                res.render('labeled-index', {
                    comments: [comments],
                    page: page,
                    pages: Math.round(count / perPage)
                })
            })
        });
});

router.get("/label", (req, res) => {
    let perPage = Math.abs(req.query.limit) || 100;
    let page = (Math.abs(req.query.page) || 1) - 1;
    // request by http://localhost:3000/label?page=1&limit=100

    Comment.find({ label: null })
        .limit(perPage)
        .skip(perPage * page)
        .sort({
            comment: 1
        })
        .exec(function (err, comments) {
            if (err) return res.redirect("/");
            Comment.countDocuments().exec(function (err, count) {
                if (err) return res.redirect("/");
                res.render('index', {
                    comments: comments,
                    page: page,
                    pages: Math.round(count / perPage)
                })
            })
        });
});

router.put("/label", function (req, res) {
    var label = req.body.label.trim();
    var id = req.body.id.trim();

    if (id == null) {
        return res.send("Id cannot be null");
    }

    if (label == -1 || label == 0 || label == 1) {
        Comment.findByIdAndUpdate(mongoose.Types.ObjectId(id),
            {
                $set: { label: label }
            }, function (err, comment) {
                if (err) {
                    console.log("err " + err);
                    return res.send(err);
                } else {
                    return res.send("Update success for comment " + comment._id);
                }
            });
    } else {
        return res.send("Wrong label");
    }
});

router.get("/labeled", (req, res) => {
    let perPage = Math.abs(req.query.limit) || 100;
    let page = (Math.abs(req.query.page) || 1) - 1;
    // request by http://localhost:3000/labeled?page=1&limit=100

    Comment.find({ label: { $ne: null } })
        .limit(perPage)
        .skip(perPage * page)
        .sort({
            comment: 1
        })
        .exec(function (err, comments) {
            if (err) return res.redirect("/");
            Comment.count().exec(function (err, count) {
                if (err) return res.redirect("/");
                res.render('labeled-index', {
                    comments: comments,
                    page: page,
                    pages: Math.round(count / perPage)
                });
            })
        });
});

router.get("/unlabeledCount", function (req, res) {
    Comment.find({ label: null }).exec(function (err, comments) {
        if (err) res.status(400).send({err});
        return res.send({size: comments.length});
    });
});

router.get("/labeledCount", function (req, res) {
    Comment.find({ label: { $ne: null } }).exec(function (err, comments) {
        if (err) res.status(400).send({err});
        return res.send({size: comments.length});
    });
});

router.get("/download", function (req, res) {
    Comment.find({ label: { $ne: null } }).exec(function (err, comments) {
        if (err) res.status(400).send({err});
        return res.send(comments);
    });
});

router.delete("/:comment_id", function (req, res) {
    console.log(req.params.product_id);
    Comment.findByIdAndRemove(req.params.comment_id, function (err, comment) {
        if (err) {
            return res.redirect("/labeled");
        } else {
            return res.redirect("/labeled");
        }
    });
});

// async function asyncCall() {
//   Product.find({}, (err, foundProducts) => {
//     if (err) {
//       console.log("err " + err);
//     } else {
//       foundProducts.forEach(async function (foundProduct) {
//         //var updateProduct = service.updateTikiProduct(foundProduct.url);
//         var totalCommentPage = await helper.getTotalCommentPage(foundProduct.url);
//         var urlCommentArray = await helper.getUrlComment(foundProduct.url, totalCommentPage);
//         var crawlComment = await service.getTikiComment(urlCommentArray.urlComments);
//       });
//     }
//   });
//   // node --max-old-space-size=16000 app.js.

//   return 1;

// }

module.exports = router;