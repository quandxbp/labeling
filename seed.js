var readXlsxFile = require('read-excel-file/node');
var Comment = require("./models/comment");

function seedDB(){
  // var categories = [
  //   { label: "010", name: "điện thoại" },
  //   { label: "011", name: "máy tính bảng" },
  //   { label: "012", name: "đồng hồ thông minh" },
  //   { label: "020", name: "tivi" },
  //   { label: "021", name: "thiết bị nghe nhìn" },
  //   { label: "022", name: "loa và phụ kiện hỗ trợ TV" },
  //   { label: "030", name: "tai nghe" },
  //   { label: "031", name: "loa bluetooth" },
  //   { label: "032", name: "phụ kiện đt" },
  //   { label: "033", name: "thiết bị game" },
  //   { label: "034", name: "thiết bị mạng" },
  //   { label: "040", name: "máy ảnh" },
  //   { label: "041", name: "máy quay phim" },
  //   { label: "042", name: "camera và phụ kiện liên quan tới camera" },
  //   { label: "050", name: "điện gia dụng" },
  //   { label: "051", name: "đồ dùng nhà bếp" },
  //   { label: "052", name: "thiết bị gia đình" },
  //   { label: "053", name: "điện lạnh" },
  //   { label: "060", name: "thiết bị chiếu sáng" },
  //   { label: "061", name: "phụ kiện nhà bếp" },
  //   { label: "062", name: "sửa chữa nhà cửa" },
  //   { label: "070", name: "vệ sinh nhà cửa" },
  //   { label: "071", name: "chăm sóc quần áo" },
  //   { label: "072", name: "chăm sóc cá nhân" },
  //   { label: "080", name: "tả,bỉm" },
  //   { label: "081", name: "sữa" },
  //   { label: "082", name: "thực phẩm cho bé" },
  //   { label: "083", name: "đồ dùng cho bé" },
  //   { label: "084", name: "đồ chơi cho bé" },
  //   { label: "085", name: "quần áo giày dép cho bé" },
  //   { label: "090", name: "làm đẹp" },
  //   { label: "091", name: "chăm sóc sức khỏe" },
  //   { label: "092", name: "chăm sóc da, trang điểm" },
  //   { label: "093", name: "dụng cụ làm đẹp" },
  //   { label: "100", name: "quần áo" },
  //   { label: "101", name: "nội y" },
  //   { label: "102", name: "giày dép" },
  //   { label: "103", name: "trang sức" },
  //   { label: "110", name: "trang phục thể thao" },
  //   { label: "111", name: "dụng cụ thể thao" },
  //   { label: "120", name: "phụ kiện xe" },
  //   { label: "121", name: "mũ bảo hiểm" },
  //   { label: "130", name: "sách tiếng Việt" },
  //   { label: "131", name: "Sách tiếng Anh" },
  //   { label: "132", name: "sách ngôn ngữ nước ngoài" }
  // ];

  // categories.forEach(function(category){
  //   Category.create(category, function(err, created) {
  //     if(err) {
  //       console.log("Err " + err);
  //     } else {
  //       console.log("create successful");
  //     }
  //   })
  // })

  // readXlsxFile('public/db_comment.xlsx').then((rows) => {
  //   rows.forEach(element => {
  //     console.log(element);
  //   });
  // });

  // var comment = {
  //   product: 'a',
  //   comment: 'b',
  //   isFemale: true,
  //   label: true
  // }

  Comment.find({} ,(err, comments) => {
    if(err) {
      console.log(err);
    }
    comments.forEach(function(comment) {
      comment.brand = "tgdd";
      comment.save();
      console.log("save comment " + comment._id + " success");
    });
    
  })
}

module.exports = seedDB;
