Separating Categories following Tiki :
010. điện thoại
011. máy tính bảng
012. đồng hồ thông minh
020. tivi
021. thiết bị nghe nhìn
022. loa và phụ kiện hỗ trợ TV
030. tai nghe
031. loa bluetooth
032. phụ kiện đt
033. thiết bị game
034. thiết bị mạng
040. máy ảnh
041. máy quay phim
042. camera và phụ kiện liên quan tới camera
050. Điện gia dụng
051. đồ dùng nhà bếp
052. thiết bị gia đình
053. điện lạnh
060. thiết bị chiếu sáng
061. phụ kiện nhà bếp
062. sửa chữa nhà cửa
070. vệ sinh nhà cửa
071. chăm sóc quần áo
072. chăm sóc cá nhân
073. chăm sóc thú cưng
080. tả,bỉm
081. sữa
082. thực phẩm cho bé
083. đồ dùng cho bé
084. đồ chơi cho bé
085. quần áo giày dép cho bé
090. làm đẹp
091. chăm sóc sức khỏe
092. chăm sóc da, trang điểm
093. dụng cụ làm đẹp
100. quần áo
101. nội y
102. giày dép
103. trang sức
110. trang phục thể thao
111. dụng cụ thể thao
120. phụ kiện xe
121. mũ bảo hiểm
130. sách tiếng Việt
131. Sách tiếng Anh
132. sách ngôn ngữ nước ngoài
