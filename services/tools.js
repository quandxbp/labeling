// var Product = require("../models/product");

// var express = require('express'),
//   async = require('async');
// router = express.Router(),
//   request = require('request'),
//   fs = require('fs'),
//   _ = require('lodash'),
//   mongoose = require('mongoose'),
//   bodyParser = require("body-parser"),
//   cheerio = require('cheerio'),
//   keys = require('../config/keys'),
//   mongo = require('mongodb'),
//   KhongDau = require('khong-dau'),
//   helper = require('../helper/support'),
//   promise = require('bluebird');

// /*
//  * FUNTIONS
//  */
// // function for crawl Tiki
// module.exports = {
//   crawlTikiUrl: function() {
//     var page = 1;
//     var per_page = 500;
//     var apikey = '2cd335e2c2c74a6f9f4b540b91128e55';

//     // config the request
//     var options = {
//       url: "https://tiki.vn/api/v2/events/deals/?sort=rand&page=" +
//         page +
//         "&per_page=" + per_page +
//         "&apikey=" + apikey
//     };

//     return new Promise((resolve, reject) => {
//       request(options, function(err, res, body) {
//         if (err) {
//           reject(err);
//         } else {
//           if (typeof counter == 'undefined') {
//             counter = 0;
//           } // Create counter variables

//           Object.preventExtensions(res);
//           res.body.slice(0, res.body.length);
//           var temp = JSON.parse(res.body);
//           temp.data.forEach(function(productData) {
//             if (productData.product != null) {
//               var url = productData.product.url_path;
//               //var currentPrice = productData.product.price;
//               url = url.split("?")[0];

//               url = "https://tiki.vn/" + url;

//               var newProduct = {
//                 url: url
//               }

//               Product.findOneAndUpdate({
//                 url: url
//               }, {
//                 $set: {
//                   url: url
//                 }
//               }, {
//                 upsert: true,
//                 new: true
//               }, (err, createdProduct) => {
//                 if (err) {
//                   reject(err);
//                 } else {
//                   counter++;
//                   console.log("Create " + counter + " success!");
//                 }
//               });
//             }
//           });
//         }
//       });
//       resolve(true);
//     });
//   },

//   getTikiComment: function(urlArray, baseUrl) {
//     // make baseUrl available in the mapping
//     // https://stackoverflow.com/questions/20882892/pass-extra-argument-to-async-map
//     async.map(urlArray, this.crawlComment, function(err, res) {
//       if (err) {
//         console.log("err");
//         return;
//       }
//     }.bind({
//       baseUrl: baseUrl
//     }), function(err, results) {
//       console.log('R', results);
//     });
//   },

//   crawlComment: async function(eachUrl, callback) {
//     await helper.requestComment(eachUrl)
//       .then((comments) => {

//         Product.findOneAndUpdate({
//           url: this.baseUrl
//         }, {
//           $addToSet: {
//             "comments": comments
//           }
//         }, (err, createdProduct) => {
//           if (err) {
//             reject(err);
//           } else {
//             callback(err, createdProduct);
//           }
//         });
//       })


//   },

//   updateTikiProduct: function(url) {
//     const options = {
//       url: url,
//       json: true
//     };
//     request(options,
//       function(err, res, body) {
//         if (typeof counter == 'undefined') {
//           counter = 0;
//         } // Create counter variables
//         if (body) {
//           var $ = cheerio.load(body);
//           if (_.includes($('title').text(), "404 Error")) {
//             console.log("404 Error");
//           } else {
//             var name = $('#product-name').text().trim();
//             var brand = $('.item-brand').find('p').find('a').html();
//             var store = $('.current-seller').find('.text').children().html();
//             var location = $('td[rel=origin]').parent();
//             location = location.find('.last').text().replace(/  /g, "").replace(/\n\n/g, "-");
//             location = location.replace(/\n/g, " ");

//             // get category of the product
//             var categoryType, categoryURL;
//             $('ul.breadcrumb').children().each(function() {
//               if ($(this).children().text().trim() === "Trang chủ") {
//                 categoryType = $(this).next().children().children().text().trim();
//                 categoryURL = KhongDau(categoryType.toLowerCase(), ["chuyen", "url"]);
//               }
//             });

//             var discountMoney = String($('#span-saving-price').text().match(/\d+/g)).replace(/,/g, ""); // So tien duoc giam
//             var currentPrice = String($('#span-price').text().match(/\d+/g)).replace(/,/g, ""); // Gia sau khi giam
//             if (currentPrice == "null") currentPrice = "0"; // check origin Price is NULL
//             var originPrice = String($('#span-list-price').text().match(/\d+/g)).replace(/,/g, ""); // Gia truoc khi giam
//             if (originPrice == "null") originPrice = "0"; // check origin Price is NULL
//             var topDescription = $('.top-feature-item').text().replace(/  /g, "").replace(/\n\n/g, "-");
//             var featureDescription = $('.white-panel').text().replace(/  /g, "").replace(/\n\n/g, "-");

//             Product.findOneAndUpdate({
//               url: options.url
//             }, {
//               $set: {
//                 name: name,
//                 brand: brand,
//                 store: store,
//                 location: location,
//                 base_category: categoryType,
//                 top_description: topDescription,
//                 feature_description: featureDescription
//               }
//             }, {
//               upsert: true,
//               new: true
//             }, (err, createdProduct) => {
//               if (err) {
//                 reject(err);
//               } else {
//                 var currentDate = new Date();

//                 if (!createdProduct.hasOwnProperty("price")) {
//                   createdProduct.price.push({
//                     current: currentPrice,
//                     origin: originPrice
//                   });
//                   createdProduct.save();
//                 } else if (createdProduct.hasOwnProperty("price") &&
//                   createdProduct.price[0].date.getDate() != currentDate.getDate()) {
//                   createdProduct.price.push({
//                     current: currentPrice,
//                     origin: originPrice
//                   });
//                   createdProduct.save();
//                   console.log("Update price");
//                 }

//                 counter++;
//                 console.log("Created product " + counter + " success!");
//               }
//             });
//           }
//         } else {
//           console.log("Product " + counter + " have no content");
//         }
//       }
//     );
//   }
// }
