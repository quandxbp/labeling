// var Product = require("../models/product");

// var express = require('express'),
//   async = require('async');
// router = express.Router(),
//   request = require('request'),
//   fs = require('fs'),
//   _ = require('lodash'),
//   mongoose = require('mongoose'),
//   rp = require('request-promise');
// bodyParser = require("body-parser"),
//   cheerio = require('cheerio'),
//   keys = require('../config/keys'),
//   mongo = require('mongodb'),
//   KhongDau = require('khong-dau'),
//   promise = require('bluebird');

// /*
//  * FUNTIONS
//  */
// // function for crawl Tiki
// module.exports = {
//   getUrlFromDBS: function() {
//     return new Promise((resolve, reject) => {
//       mongoose.connect(keys.mongoURI, function(err, db) {
//         var collectionName = db.collection('products');
//         var urlArray = [];
//         collectionName.find("url").toArray(function(err, urlArray) {
//           if (err) throw err;
//           else {
//             for (var i = 0; i < urlArray.length; i++) {
//               urlArray[i] = "https://tiki.vn/" + urlArray[i].url;
//             }
//             resolve({
//               url: urlArray
//             });
//           }
//         });
//       });
//     });
//   },

//   getTotalCommentPage: function(url) {
//     var productCode = url.split("-p")[1];
//     productCode = productCode.split(".html")[0];
//     var apikey = '2cd335e2c2c74a6f9f4b540b91128e55';
//     var newUrl = "https://tiki.vn/api/v2/reviews?product_id=" + productCode +
//       "&limit=5&sort=score|desc,id|desc,stars|all&include=comments&page=1&apikey=" + apikey;
//     return new Promise(function(resolve, reject) {
//       request(newUrl, function(err, res, body) {
//         if (err) {
//           reject(err);
//         } else {
//           Object.preventExtensions(res);
//           res.body.slice(0, res.body.length);
//           var temp = JSON.parse(res.body);
//           var totalPage = temp.paging.last_page;
//           resolve(totalPage);
//         }
//       });
//     });
//   },

//   getUrlComment: function(url, totalCommentPage) {
//     return new Promise((resolve, reject) => {
//       //console.log(url," ",totalCommentPage);
//       var productCode = url.split("-p")[1];
//       productCode = productCode.split(".html")[0];
//       var apikey = '2cd335e2c2c74a6f9f4b540b91128e55';
//       var urlArray = [];
//       // save the request to tiki that get the comments to array
//       if (totalCommentPage == 0 || totalCommentPage > 120000) {
//         urlArray.push("https://tiki.vn/api/v2/reviews?product_id=" + productCode +
//           "&limit=5&sort=score|desc,id|desc,stars|all&include=comments&page=1&apikey=" + apikey);
//       }
//       else {
//         for (var i = 1; i <= totalCommentPage; i++) {
//           eachUrl = "https://tiki.vn/api/v2/reviews?product_id=" + productCode +
//             "&limit=5&sort=score|desc,id|desc,stars|all&include=comments&page=" + i +
//             "&apikey=" + apikey;
//           urlArray.push(eachUrl);
//         }
//       }
//       // return that array
//       resolve({
//         urlComments: urlArray
//       });
//     })
//   },

//   requestComment: async function(eachUrl) {
//     return new Promise((resolve, reject) => {
//       var comments = [];

//       const options = {
//         url: eachUrl
//       };
//       // request each URL in page
//       request(options, function(err, res, body) {
//         console.log(options);
//         Object.preventExtensions(res);
//         res.body.slice(0, res.body.length);
//         // the request failed; tiki often fail for comment request
//         if(res.body.substring(0,15) != "<!doctype html>") {
//           var temp = JSON.parse(res.body);
//           averageRating = temp.rating_average;

//           temp.data.forEach(function(productComment) {
//             comment = {
//               author_name: productComment.created_by.name,
//               title: productComment.title,
//               content: productComment.content,
//               location: productComment.created_by.region,
//               is_buy: productComment.created_by.purchased
//             }
//             comments.push(comment);
//           });

//           resolve(comments);
//         }
//         else {
//           console.log("Load fail");
//         }
//       });
//     })
//   }

// }
