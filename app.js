var express = require('express'),
  app = express(),
  bodyParser = require("body-parser"),
  mongoose = require("mongoose"),
  methodOverride = require("method-override"),
  keys = require('./config/keys'),
  seedDB = require("./seed"),
  morgan = require('morgan');

var index = require("./routes/index");

// Connect the Mongo Mlab database
mongoose.connect(keys.DATABASEURL,  { useNewUrlParser: true })
  .then(() => {
    console.log("Connected to DB");
  }).catch(err => console.error(err));

// set up view engine .ejs
app.set("view engine", "ejs");

// expose the public folder
app.use(express.static("public"));

// parse the body
app.use(bodyParser.urlencoded({ extended: true }));

// for PUT and delete request
app.use(methodOverride("_method"));

// use morgan to log requests to the console
app.use(morgan('dev'));

// main route
app.use("/", index);

// uncomment to create more category
//seedDB();

// handling error; throw new Error('oops')
app.use((err, request, response, next) => {
  // log the error, for now just console.log
  console.log(err);
  response.status(500).send('Something broke!');
});

// start the server, change the world .
app.listen(process.env.PORT || 3000, process.env.IP, (err) => {
  if (err) {
    return console.log('something bad happened', err);
  }

  console.log("Starting the server! at PORT " + process.env.PORT + " and IP " + process.env.IP);
});