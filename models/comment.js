var mongoose = require("mongoose");

var CommentSchema = new mongoose.Schema({
  product: String,
  comment: String,
  isFemale: Boolean,
  isPurchased: Boolean,
  brand: String,
  label: Number, // positive or negative
});

module.exports = mongoose.model("Comment", CommentSchema);
